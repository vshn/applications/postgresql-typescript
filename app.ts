import { Application, Router } from "https://deno.land/x/oak@v12.1.0/mod.ts"
import { Handlebars } from "https://deno.land/x/handlebars@v0.9.0/mod.ts"
import { Client } from "https://deno.land/x/postgres@v0.17.0/mod.ts"

interface Movie {
  title: string
  director: string
  release_date: string
  imdb: string
  len: string
}

const app = new Application()
const router = new Router()
const hbs = new Handlebars()

// Connect to database and retrieve data
// The connection information is provided via standard environment variables
// https://www.postgresql.org/docs/14/libpq-envars.html
const client = new Client()
await client.connect()
const query = `SELECT title, concat(d.first_name, ' ', d.last_name) as director,
  to_char(release_date, 'Dy DD Mon YYYY') as release_date, imdb, len
  FROM films
  INNER JOIN directors AS d
  ON films.director_id = d.id
  ORDER BY release_date`
const movies = await client.queryObject<Movie>(query)
await client.end()

// Router
router.get(`/`, async (ctx) => {
  const data = {
    films: movies.rows,
  }
  ctx.response.body = await hbs.renderView("index", data)
})

// Start application
app.use(router.routes())
console.log(`Now listening on http://0.0.0.0:8080`)
await app.listen("0.0.0.0:8080")
